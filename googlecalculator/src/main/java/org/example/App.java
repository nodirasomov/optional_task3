package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * Hello world!
 *
 */
public class App 
{
    WebDriver webDriver;
    WebDriverWait wait;



    String text = "Google Cloud Pricing Calculator";

    public App() {
         webDriver=new ChromeDriver();
         wait = new WebDriverWait(webDriver, Duration.ofSeconds(30));

    };

    By SearchButton = By.xpath("//*[@id='kO001e']/div[2]/div[1]/div/div[2]/div[2]/div[1]");

    By SearchField = By.xpath("//*[@id='i4']");

    By LinkOfCalculatorPage = By.cssSelector("[href$='products/calculator']");

    By AddEstimateButton = By.xpath("//span[@class='UywwFc-vQzf8d']");

    By ComputeEngineTab = By.xpath("//div[@class='d5NbRd-EScbFb-JIbuQc PtwYlf']//h2[text()='Compute Engine']");

    By NumberOfInstance = By.xpath("//div[@class='QiFlid']//input[@class='qdOxv-fmcmS-wGMbrd']");


    By MachineType = By.xpath("//div[@aria-labelledby='c31 c33']");

    By MachineTypeOption = By.xpath("//li[@data-value='n1-standard-8']");


    By AddGPUsButton = By.xpath("//button[@aria-label='Add GPUs']");

    By LocalSSD = By.xpath("//div[@placeholder-id='ucc-72']");


    By LocalSSDOption = By.xpath("//div[@data-menu-uid='ucc-75']//li[3]");

    By Region = By.xpath("//div[@aria-labelledby='c43 c45']");

    By SelectedRegion = By.xpath("//li[@data-value='europe-west3']");

    By CommittedUsage = By.xpath("//label[@for='1-year']");

    By OpenShareButton = By.xpath("//div[@data-is-touch-wrapper='true']//button[@data-idom-class=' VKmq8b AXySwf']");

    By OpenEstimateSummary = By.xpath("//*[@class='MPvxGc NMm5M hhikbc']");


    public void openPageAndEnterWords() {

        webDriver.get("https://cloud.google.com/");

        webDriver.findElement(SearchButton).click();

        webDriver.findElement(SearchField).sendKeys(text);

        webDriver.findElement(SearchField).sendKeys(Keys.ENTER);

        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(LinkOfCalculatorPage));
        element.click();

    }

    public void openComputeEngineAndAddKeywords() {

        webDriver.findElement(AddEstimateButton).click();

        WebElement element2 = wait.until(ExpectedConditions.elementToBeClickable(ComputeEngineTab));
        element2.click();

        WebElement element3 = wait.until(ExpectedConditions.elementToBeClickable(NumberOfInstance));
        element3.clear();
        element3.sendKeys("4");

        WebElement element4 = wait.until(ExpectedConditions.elementToBeClickable(MachineType));
        element4.click();

        webDriver.findElement(MachineTypeOption).click();
        webDriver.findElement(AddGPUsButton).click();

        WebElement element5 = wait.until(ExpectedConditions.elementToBeClickable(LocalSSD));
        element5.click();


        WebElement element6 = wait.until(ExpectedConditions.elementToBeClickable(LocalSSDOption));
        element6.click();

        WebElement element7 = wait.until(ExpectedConditions.elementToBeClickable(Region));
        element7.click();

        WebElement element8 = wait.until(ExpectedConditions.elementToBeClickable(SelectedRegion));
        element8.click();

        webDriver.findElement(CommittedUsage).click();

    }

    public void openShareEstimateTab() {

        webDriver.findElement(OpenShareButton).click();

        WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(OpenShareButton));
        webElement.click();

        webDriver.findElement(OpenEstimateSummary).click();




    }
}





